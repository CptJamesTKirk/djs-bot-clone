const vm = require("vm2");

module.exports.execute = async (client, message, args) => {
   
    // Create vm object
    let runner = new vm({
        timeout: 10000,
        sandbox: {}
    });

    let result;

    // Safely execute the code
    try {
        result = runner.run(args.join(' ')).toString();
    }
    catch (e) {
        result = e.message;
    }

    await message.channel.send('`' + result + '`');

};
  
module.exports.config = {
    name: 'safeeval',
    description: 'Safely evals code and returns the result'
};