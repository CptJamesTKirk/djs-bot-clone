module.exports.execute = async (client, message, args) => {
  const Discord = require('discord.js');
  if (message.author.id !== '278620217221971968') return;
  let evaled;
  try {
    evaled = await eval(args.join(' '));
  } catch (err) {
    const embed = new Discord.RichEmbed();
    embed.setTitle('JavaScript Eval');
    embed.setColor('RED');
    embed.setDescription(`[Error]\n \`\`\`js\n${err}\`\`\``);
    embed.setFooter(client.user.username, client.user.avatarURL);
    embed.setTimestamp();
    return message.channel.send(embed);
  }

  if (typeof evaled === 'string') {
    evaled = evaled.replace(client.token, '[TOKEN]');
  }


  if (typeof evaled === 'object') {
    evaled = require('util').inspect(evaled, {depth: 0});
  }
  if (evaled == undefined) {
    evaled = 'undefined';
  }

  const embed = new Discord.RichEmbed();
  embed.setTitle('JavaScript Eval');
  embed.setColor('GREEN');
  embed.setDescription(`\`\`\`js\n${evaled}\`\`\``);
  embed.setFooter(client.user.username, client.user.avatarURL);
  embed.setTimestamp();
  message.channel.send(embed);
};

module.exports.config = {
  name: 'eval',
  description: 'Evaluates JS code.'
};