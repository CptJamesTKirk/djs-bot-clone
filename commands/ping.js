module.exports.execute = async (client, message) => {
  const msg = await message.channel.send('🏓 Pong!');
      
  msg.edit(`🏓 Pong! \`${msg.createdTimestamp - message.createdTimestamp}ms\``);
    
};

module.exports.config = {
  name: 'ping',
  description: 'Pings the bot.'
};