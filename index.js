const fs = require('fs');
const Discord = require('discord.js');
const { prefix, token } = require('./config.json');

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.config.name, command);
}

client.once('ready', () => {
  console.log(`${client.user.tag} is ready in ${client.guilds.size} servers with ${client.users.size} users!`);
  client.user.setActivity('LOC Discord.js Event', { type: 'WATCHING' });
});

client.on('message', message => {
  if (!message.content.startsWith(prefix) || message.author.bot) return;

  const args = message.content.slice(prefix.length).split(/ +/);
  const command = args.shift().toLowerCase();
  const cmd = client.commands.get(command);

  if (!client.commands.has(command)) return;

  try {
    cmd.execute(client, message, args);
  } catch (error) {
    console.error(error);
    message.reply('there was an error trying to execute that command!');
  }
});

client.login(token);
